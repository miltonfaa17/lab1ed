
package eje3lab1;


public class nodolista {
    Object valor;
    nodolista siguiente;
    
    public nodolista(Object valor){
        this.valor = valor;
        this.siguiente = null;
    }
    
    public Object obtenrvalor(){
        return valor;
    }
    
    public void enlazarSiguiente(nodolista n){
        siguiente = n;
    }
    
    public nodolista obtenerSiguiente(){
        return siguiente;
    }
}
