
package eje1lab1;

import java.util.ArrayList;

public class cola {
    private ArrayList<Object> colap = new ArrayList();
    
    public void offer(Object c){
        colap.add(c);
    }
    
    public Object peek(){
        if (!(colap.isEmpty())){
        return colap.get(0);
    }else{
            return null;
        }
    }
    
    public Object poll(){
        if (!(colap.isEmpty())){
            Object c = colap.get(0);
            colap.remove(0);
            return c;
        }else{
            return null;
        }
    }
}
