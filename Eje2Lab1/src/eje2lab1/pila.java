
package eje2lab1;

import java.util.ArrayList;

public class pila {
    private ArrayList<Object> filo = new ArrayList();
    
    public void push(Object a){
        filo.add(a);
    }
    
    public Object pop(){
        if (!(filo.isEmpty())){
            Object x = filo.get(filo.size()-1);
            filo.remove(filo.size()-1);
            return x;
        }else{
            return "pila vacia";
        }
    }
    
    public Object peek(){
        if (!(filo.isEmpty())){
            return filo.get(filo.size()-1);
        }else{
            return "pila vacia";
        }
    }
    
    public Boolean empty(){
        return filo.isEmpty();
    }
}
